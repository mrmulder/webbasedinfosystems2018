package nl.bioinf.wis1.dao;

import nl.bioinf.wis1.users.User;

public interface MyUserDatasource {
    void connect() throws MyDataSourceException;
    void disconnect() throws MyDataSourceException;
    User getUser(String userName, String userPassword) throws MyDataSourceException;
    void insertUser(User newUser) throws MyDataSourceException;
}

package nl.bioinf.wis1.dao;

import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;

import java.io.IOException;

public class DataSourceFactory {
    public static MyUserDatasource getDataSource(String type) {
        //type can be mysql or dummy
        switch (type) {
            case "dummy": return MyDataSourceDummy.getInstance();
            case "mysql": {
                if(! MyDataSourceMysql.hasInstance()) {
                    DbUser dbUser = null;
                    try {
                        dbUser = DbCredentials.getMySQLuser();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                    MyDataSourceMysql.createInstance(dbUser.getHost(),
                            dbUser.getUserName(),
                            dbUser.getDatabasePassword(),
                            dbUser.getDatabaseName());
                }
                return MyDataSourceMysql.getInstance();}
            default: throw new IllegalArgumentException("no such DB type: " + type);
        }
    }
}

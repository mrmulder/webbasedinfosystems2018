package nl.bioinf.wis1.dao;

import nl.bioinf.wis1.users.Address;
import nl.bioinf.wis1.users.User;

import java.util.HashMap;
import java.util.Map;

public class MyDataSourceDummy implements MyUserDatasource {
    private Map<String, User> myDummyDb = new HashMap<>();

    /*
     * singleton part 1
     */
    private static MyDataSourceDummy singleInstance;

    /*
     * singleton part 2
     */
    public static MyUserDatasource getInstance() {
        if (singleInstance == null) {
            singleInstance = new MyDataSourceDummy();
        }
        return singleInstance;
    }

    /*
     * singleton part 3
     */
    private MyDataSourceDummy() {}

    @Override
    public void connect() throws MyDataSourceException {
        User u = new User("Henkie", "Henkie");
        u.setEmail("Henkiehenk@example.com");
        Address address = new Address(7, "a", "9701 DA", "Zernikeplein", "Groningen");
        u.setAddress(address);
        myDummyDb.put(u.getUserName(), u);
    }

    @Override
    public void disconnect() throws MyDataSourceException {
        myDummyDb.clear();
    }

    @Override
    public User getUser(String userName, String userPassword) throws MyDataSourceException {
        if (myDummyDb.containsKey(userName)
                && myDummyDb.get(userName).getPassWord().equals(userPassword)) {
            return myDummyDb.get(userName);
        } else {
            throw new MyDataSourceException("No user found with these credentials: name=" + userName);
        }
    }

    @Override
    public void insertUser(User newUser) throws MyDataSourceException {
        if (myDummyDb.containsKey(newUser.getUserName())) {
            throw new MyDataSourceException("User already exists: " + newUser.getUserName());
        } else {
            myDummyDb.put(newUser.getUserName(), newUser);
        }
    }

}

<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 23/11/2017
  Time: 15:09
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
    <body>
        <h4>Log in</h4>
        <h4>${requestScope.login_error}</h4>
        <form action="login.do" method="POST">
            <label for="#username_field"> User name: </label>
            <input id="username_field" type="text" name="username" required/> <br/>
            <label for="#password_field"> User password: </label>
            <input id="password_field" type="password" name="password" required/><br/>
            <label class="login_field"> </label>
            <input type="submit" value="OK"/>
        </form>

    <h4>Problems logging in? Please contact admin at ${initParam.admin_email}</h4>
    </body>
</html>

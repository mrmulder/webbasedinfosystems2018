//var url = "www.bioinf.nl/~michiel" BAD IDEA
var MY_CONFIG= {

}

/* Initialization on window loading
* Uses an anonymous function to avoid cluttering the global namespace*/
window.onload = function(){
    var messages = ["Try again", "oops missed", "wow are you blind?", "this is getting emberrassing"];
    var currentMessageIndex = 0;

    function relocate(){
        var width = window.innerWidth;
        var height = window.innerHeight;
        var newLeft = Math.random() * width;
        var newTop = Math.random() * height;
        document.getElementById("unclickable").style.left = newLeft + "px";
        document.getElementById("unclickable").style.top = newTop + "px";
        $("#unclickable").css("font-size", "20px");
        $("#unclickable").text(messages[currentMessageIndex++]);
        console.log("current index = " + currentMessageIndex);
        if (currentMessageIndex == messages.length) {
            currentMessageIndex = 0;
        }
    };
//relocate when the user attempts to click
    document.getElementById("unclickable").onmouseover = relocate;
};

var MyAwesomeModule = (function(){
    var current = 1;
    function privateMethod() {
        console.log("private private");
    }

    var privateMethodTwo = function() {
        console.log("private two")
    }

    function init(){
        console.log("init");
        privateMethod();
        privateMethodTwo();
        current++;
        console.log("current=" + current)
    }
    function change(){
        console.log("change");
    }
    function verify(){
        console.log("verify");
    }
    return {
        startUp: init
    };
})();

MyAwesomeModule.startUp();
MyAwesomeModule.startUp();
MyAwesomeModule.startUp();
